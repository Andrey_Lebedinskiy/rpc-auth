package service

import (
	"context"
	userPB "gitlab.com/Andrey_Lebedinskiy/my-go-rpc/grpc/proto/user"
	"gitlab.com/Andrey_Lebedinskiy/rpc-auth/internal/infrastructure/errors"
	"gitlab.com/Andrey_Lebedinskiy/rpc-auth/internal/models"
)

type UserServiceGRPC struct {
	client userPB.UserClient
}

func NewUserServiceGRPC(client userPB.UserClient) Userer {
	return &UserServiceGRPC{client: client}
}

func (u *UserServiceGRPC) Create(ctx context.Context, in UserCreateIn) UserCreateOut {
	res, err := u.client.Create(ctx, &userPB.CreateUserRequest{
		Name:     in.Name,
		Phone:    in.Phone,
		Email:    in.Email,
		Password: in.Password,
		Role:     int64(in.Role),
	})
	if err != nil {
		return UserCreateOut{
			UserID:    int(res.UserID),
			ErrorCode: errors.UserServiceCreateUserErr,
		}
	}

	return UserCreateOut{
		UserID:    int(res.UserID),
		ErrorCode: int(res.ErrorCode),
	}
}

func (u *UserServiceGRPC) Update(ctx context.Context, in UserUpdateIn) UserUpdateOut {
	fields := make([]int64, 0, len(in.Fields))
	for _, elem := range in.Fields {
		fields = append(fields, int64(elem))
	}
	user := &userPB.UserIn{
		ID:            int64(in.User.ID),
		Name:          in.User.Name,
		Phone:         in.User.Phone,
		Email:         in.User.Email,
		Password:      in.User.Password,
		Role:          int64(in.User.Role),
		Verified:      in.User.Verified,
		EmailVerified: in.User.EmailVerified,
		PhoneVerified: in.User.PhoneVerified,
	}

	res, err := u.client.Update(ctx, &userPB.UpdateUserRequest{
		User:   user,
		Fields: fields,
	})
	if err != nil {
		return UserUpdateOut{
			Success:   false,
			ErrorCode: errors.UserServiceUpdateErr,
		}
	}

	return UserUpdateOut{
		Success:   res.Success,
		ErrorCode: int(res.ErrorCode),
	}

}

func (u *UserServiceGRPC) VerifyEmail(ctx context.Context, in UserVerifyEmailIn) UserUpdateOut {
	res, err := u.client.VerifyEmail(ctx, &userPB.VerifyEmailRequest{UserID: int64(in.UserID)})
	if err != nil {
		return UserUpdateOut{
			Success:   res.Success,
			ErrorCode: errors.GeneralError,
		}
	}

	return UserUpdateOut{
		Success:   res.Success,
		ErrorCode: int(res.ErrorCode),
	}
}

func (u *UserServiceGRPC) ChangePassword(ctx context.Context, in ChangePasswordIn) ChangePasswordOut {
	res, err := u.client.ChangePassword(ctx, &userPB.ChangePasswordRequest{
		UserID:      int64(in.UserID),
		OldPassword: in.OldPassword,
		NewPassword: in.NewPassword,
	})
	if err != nil {
		return ChangePasswordOut{
			Success:   false,
			ErrorCode: errors.UserServiceChangePasswordErr,
		}
	}

	return ChangePasswordOut{
		Success:   res.Success,
		ErrorCode: int(res.ErrorCode),
	}

}

func (u *UserServiceGRPC) GetByEmail(ctx context.Context, in GetByEmailIn) UserOut {
	res, err := u.client.GetByEmail(ctx, &userPB.GetByEmailRequest{Email: in.Email})
	if err != nil {
		return UserOut{
			User:      nil,
			ErrorCode: errors.GeneralError,
		}
	}

	user := &models.User{
		ID:            int(res.User.ID),
		Name:          res.User.Name,
		Phone:         res.User.Phone,
		Email:         res.User.Email,
		Password:      res.User.Password,
		Role:          int(res.User.Role),
		Verified:      res.User.Verified,
		EmailVerified: res.User.EmailVerified,
		PhoneVerified: res.User.PhoneVerified,
	}
	return UserOut{
		User:      user,
		ErrorCode: int(res.ErrorCode),
	}
}

func (u *UserServiceGRPC) GetByPhone(ctx context.Context, in GetByPhoneIn) UserOut {
	res, err := u.client.GetByPhone(ctx, &userPB.GetByPhoneRequest{Phone: in.Phone})
	if err != nil {
		return UserOut{
			User:      nil,
			ErrorCode: errors.GeneralError,
		}
	}

	user := &models.User{
		ID:            int(res.User.ID),
		Name:          res.User.Name,
		Phone:         res.User.Phone,
		Email:         res.User.Email,
		Password:      res.User.Password,
		Role:          int(res.User.Role),
		Verified:      res.User.Verified,
		EmailVerified: res.User.EmailVerified,
		PhoneVerified: res.User.PhoneVerified,
	}
	return UserOut{
		User:      user,
		ErrorCode: int(res.ErrorCode),
	}
}
func (u *UserServiceGRPC) GetByID(ctx context.Context, in GetByIDIn) UserOut {
	res, err := u.client.GetByID(ctx, &userPB.GetByIDRequest{UserID: int64(in.UserID)})
	if err != nil {
		return UserOut{
			User:      nil,
			ErrorCode: errors.UserServiceRetrieveUserErr,
		}
	}

	user := &models.User{
		ID:            int(res.User.ID),
		Name:          res.User.Name,
		Phone:         res.User.Phone,
		Email:         res.User.Email,
		Password:      res.User.Password,
		Role:          int(res.User.Role),
		Verified:      res.User.Verified,
		EmailVerified: res.User.EmailVerified,
		PhoneVerified: res.User.PhoneVerified,
	}
	return UserOut{
		User:      user,
		ErrorCode: int(res.ErrorCode),
	}
}

func (u *UserServiceGRPC) GetByIDs(ctx context.Context, in GetByIDsIn) UsersOut {
	ids := make([]int64, 0, len(in.UserIDs))
	for _, elem := range in.UserIDs {
		ids = append(ids, int64(elem))
	}
	res, err := u.client.GetByIDs(ctx, &userPB.GetByIDsRequest{UserIDs: ids})
	if err != nil {
		return UsersOut{
			User:      nil,
			ErrorCode: errors.UserServiceRetrieveUsersErr,
		}
	}

	users := make([]models.User, 0, len(res.Users))
	for _, elem := range res.Users {
		users = append(users, models.User{
			ID:            int(elem.ID),
			Name:          elem.Name,
			Phone:         elem.Phone,
			Email:         elem.Email,
			Password:      elem.Password,
			Role:          int(elem.Role),
			Verified:      elem.Verified,
			EmailVerified: elem.EmailVerified,
			PhoneVerified: elem.PhoneVerified,
		})
	}
	return UsersOut{
		User:      users,
		ErrorCode: int(res.ErrorCode),
	}
}
