package main

import (
	"github.com/joho/godotenv"
	"gitlab.com/Andrey_Lebedinskiy/rpc-auth/config"
	"gitlab.com/Andrey_Lebedinskiy/rpc-auth/internal/infrastructure/logs"
	"gitlab.com/Andrey_Lebedinskiy/rpc-auth/run"
	"os"
)

func main() {
	err := godotenv.Load()
	conf := config.NewAppConf()
	logger := logs.NewLogger(conf, os.Stdout)
	if err != nil {
		logger.Fatal("error while loading .env file")
	}

	conf.Init(logger)

	app := run.NewApp(conf, logger)

	exitCode := app.Bootstrap().Run()
	os.Exit(exitCode)
}
