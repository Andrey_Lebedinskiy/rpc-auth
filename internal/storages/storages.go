package storages

import (
	"gitlab.com/Andrey_Lebedinskiy/rpc-auth/internal/db/adapter"
	vstorage "gitlab.com/Andrey_Lebedinskiy/rpc-auth/internal/modules/auth/storage"
	//ustorage "gitlab.com/Andrey_Lebedinskiy/rpc-auth/internal/modules/auth/storages"
)

type Storages struct {
	//User   ustorage.Userer
	Verify vstorage.Verifier
}

func NewStorages(sqlAdapter *adapter.SQLAdapter) *Storages {
	return &Storages{
		//User:   ustorage.NewUserStorage(sqlAdapter, cache),
		Verify: vstorage.NewEmailVerify(sqlAdapter),
	}
}
