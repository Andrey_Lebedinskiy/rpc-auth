package Auth

import (
	"context"
	"gitlab.com/Andrey_Lebedinskiy/rpc-auth/internal/modules/auth/service"
	uservice "gitlab.com/Andrey_Lebedinskiy/rpc-auth/internal/modules/user/service"
)

// AuthServiceJSONRPC представляет AuthService для использования в JSON-RPC
type AuthServiceJSONRPC struct {
	AuthService service.Auther
}

// NewAuthServiceJSONRPC возвращает новый AuthServiceJSONRPC
func NewAuthServiceJSONRPC(AuthService service.Auther) *AuthServiceJSONRPC {
	return &AuthServiceJSONRPC{AuthService: AuthService}
}

func (a *AuthServiceJSONRPC) Register(in service.RegisterIn, field int, out *service.RegisterOut) error {
	*out = a.AuthService.Register(context.Background(), in, field)
	return nil
}

func (a *AuthServiceJSONRPC) AuthorizeEmail(in service.AuthorizeEmailIn, out *service.AuthorizeOut) error {
	*out = a.AuthService.AuthorizeEmail(context.Background(), in)
	return nil
}

func (a *AuthServiceJSONRPC) AuthorizeRefresh(in service.AuthorizeRefreshIn, out *service.AuthorizeOut) error {
	*out = a.AuthService.AuthorizeRefresh(context.Background(), in)
	return nil
}

func (a *AuthServiceJSONRPC) AuthorizePhone(in service.AuthorizePhoneIn, out *service.AuthorizeOut) error {
	*out = a.AuthService.AuthorizePhone(context.Background(), in)
	return nil
}

func (a *AuthServiceJSONRPC) SendPhoneCode(in service.SendPhoneCodeIn, out *service.SendPhoneCodeOut) error {
	*out = a.AuthService.SendPhoneCode(context.Background(), in)
	return nil
}

func (a *AuthServiceJSONRPC) VerifyEmail(in service.VerifyEmailIn, out *service.VerifyEmailOut) error {
	*out = a.AuthService.VerifyEmail(context.Background(), in)
	return nil
}

func (a *AuthServiceJSONRPC) SetUserer(user uservice.Userer) error {
	a.AuthService.SetUserer(user)
	return nil
}
