package run

import (
	"context"
	"fmt"
	jsoniter "github.com/json-iterator/go"
	"github.com/ptflp/godecoder"
	authPB "gitlab.com/Andrey_Lebedinskiy/my-go-rpc/grpc/proto/auth"
	userPB "gitlab.com/Andrey_Lebedinskiy/my-go-rpc/grpc/proto/user"
	"gitlab.com/Andrey_Lebedinskiy/rpc-auth/config"
	"gitlab.com/Andrey_Lebedinskiy/rpc-auth/internal/db"
	"gitlab.com/Andrey_Lebedinskiy/rpc-auth/internal/infrastructure/component"
	"gitlab.com/Andrey_Lebedinskiy/rpc-auth/internal/infrastructure/db/migrate"
	"gitlab.com/Andrey_Lebedinskiy/rpc-auth/internal/infrastructure/db/scanner"
	"gitlab.com/Andrey_Lebedinskiy/rpc-auth/internal/infrastructure/errors"
	"gitlab.com/Andrey_Lebedinskiy/rpc-auth/internal/infrastructure/responder"
	"gitlab.com/Andrey_Lebedinskiy/rpc-auth/internal/infrastructure/server"
	internal "gitlab.com/Andrey_Lebedinskiy/rpc-auth/internal/infrastructure/service"
	"gitlab.com/Andrey_Lebedinskiy/rpc-auth/internal/infrastructure/tools/cryptography"
	"gitlab.com/Andrey_Lebedinskiy/rpc-auth/internal/models"
	modules2 "gitlab.com/Andrey_Lebedinskiy/rpc-auth/internal/modules"
	client "gitlab.com/Andrey_Lebedinskiy/rpc-auth/internal/modules/user/service"
	"gitlab.com/Andrey_Lebedinskiy/rpc-auth/internal/provider"
	storages "gitlab.com/Andrey_Lebedinskiy/rpc-auth/internal/storages"
	Auth "gitlab.com/Andrey_Lebedinskiy/rpc-auth/rpc/auth"
	"go.uber.org/zap"
	"golang.org/x/sync/errgroup"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"net/rpc"
	"net/rpc/jsonrpc"
	"os"
)

// Application - интерфейс приложения
type Application interface {
	Runner
	Bootstraper
}

// Runner - интерфейс запуска приложения
type Runner interface {
	Run() int
}

// Bootstraper - интерфейс инициализации приложения
type Bootstraper interface {
	Bootstrap(options ...interface{}) Runner
}

// App - структура приложения
type App struct {
	conf   config.AppConf
	logger *zap.Logger
	//srv      server.Server
	jsonRPC  server.Server
	Sig      chan os.Signal
	Storages *storages.Storages
	Servises *modules2.Services
}

// NewApp - конструктор приложения
func NewApp(conf config.AppConf, logger *zap.Logger) *App {
	return &App{conf: conf, logger: logger, Sig: make(chan os.Signal, 1)}
}

// Run - запуск приложения
func (a *App) Run() int {
	// на русском
	// создаем контекст для graceful shutdown
	ctx, cancel := context.WithCancel(context.Background())

	errGroup, ctx := errgroup.WithContext(ctx)

	// запускаем горутину для graceful shutdown
	// при получении сигнала SIGINT
	// вызываем cancel для контекста
	errGroup.Go(func() error {
		sigInt := <-a.Sig
		a.logger.Info("signal interrupt recieved", zap.Stringer("os_signal", sigInt))
		cancel()
		return nil
	})

	// запускаем http сервер
	//errGroup.Go(func() error {
	//	err := a.srv.Serve(ctx)
	//	if err != nil && err != http.ErrServerClosed {
	//		a.logger.Error("app: server error", zap.Error(err))
	//		return err
	//	}
	//	return nil
	//})

	// запускаем json rpc сервер
	//errGroup.Go(func() error {
	//	err := a.jsonRPC.Serve(ctx)
	//	if err != nil {
	//		a.logger.Error("app: server error", zap.Error(err))
	//		return err
	//	}
	//	return nil
	//})

	if err := errGroup.Wait(); err != nil {
		return errors.GeneralError
	}

	//запускаем сервер
	go func() {
		err := a.jsonRPC.Serve(context.Background())
		if err != nil {
			a.logger.Fatal("app: server error", zap.Error(err))
		}
	}()

	return errors.NoError
}

// Bootstrap - инициализация приложения
func (a *App) Bootstrap(options ...interface{}) Runner {
	// на русском
	// инициализация емейл провайдера
	email := provider.NewEmail(a.conf.Provider.Email, a.logger)
	// инициализация сервиса нотификации
	notifyEmail := internal.NewNotify(a.conf.Provider.Email, email, a.logger)
	// инициализация менеджера токенов
	tokenManager := cryptography.NewTokenJWT(a.conf.Token)
	// инициализация декодера
	decoder := godecoder.NewDecoder(jsoniter.Config{
		EscapeHTML:             true,
		SortMapKeys:            true,
		ValidateJsonRawMessage: true,
		DisallowUnknownFields:  true,
	})
	// инициализация менеджера ответов сервера
	responseManager := responder.NewResponder(decoder, a.logger)
	// инициализация генератора uuid
	uuID := cryptography.NewUUIDGenerator()
	// инициализация хешера
	hash := cryptography.NewHash(uuID)
	// инициализация компонентов
	components := component.NewComponents(a.conf, notifyEmail, tokenManager, responseManager, decoder, hash, a.logger)
	// инициализация сканера таблиц
	tableScanner := scanner.NewTableScanner()
	// регистрация таблиц
	tableScanner.RegisterTable(
		&models.UserDTO{},
		&models.EmailVerifyDTO{},
		&models.TestDTO{},
	)
	// инициализация базы данных sql и его адаптера
	dbx, sqlAdapter, err := db.NewSqlDB(a.conf.DB, tableScanner, a.logger)
	if err != nil {
		a.logger.Fatal("error init db", zap.Error(err))
	}
	// инициализация мигратора
	migrator := migrate.NewMigrator(dbx, a.conf.DB, tableScanner)
	err = migrator.Migrate()
	if err != nil {
		a.logger.Fatal("migrator err", zap.Error(err))
	}

	// инициализация хранилищ
	newStorages := storages.NewStorages(sqlAdapter)
	a.Storages = newStorages

	var services *modules2.Services

	// инициализация сервисов в зависимости от выбранного типа связи
	//для упрощения будем считать, что у клиента и сервера совпадают технологии обмена данными
	switch a.conf.RPCServer.Type {
	case "JSON_RPC":
		clientUser, err := jsonrpc.Dial("tcp", fmt.Sprintf("%s:%s", a.conf.UserRPC.Host, a.conf.UserRPC.Port))
		if err != nil {
			a.logger.Fatal("error init json-rpc-user client", zap.Error(err))
		}
		a.logger.Info("rpc-user client connected")

		user := client.NewUserServiceJSONRPC(clientUser)
		services = modules2.NewServices(newStorages, components, user)
	case "GRPC":
		conn, err := grpc.Dial(fmt.Sprintf(
			"%s:%s", a.conf.UserRPC.Host, a.conf.UserRPC.Port),
			grpc.WithTransportCredentials(insecure.NewCredentials()))
		if err != nil {
			a.logger.Fatal("error init grpc-user client", zap.Error(err))
		}
		a.logger.Info("grpc-user client connected")

		user := client.NewUserServiceGRPC(userPB.NewUserClient(conn))
		services = modules2.NewServices(newStorages, components, user)
	default:
		a.logger.Fatal("unknown client type")
	}

	a.Servises = services

	// инициализация сервера в зависимости от реализации
	switch a.conf.RPCServer.Type {
	case "JSON_RPC":
		authRPC := Auth.NewAuthServiceJSONRPC(a.Servises.Auth)
		jsonRPCServer := rpc.NewServer()
		err = jsonRPCServer.Register(authRPC)
		if err != nil {
			a.logger.Fatal("error init auth json RPC", zap.Error(err))
		}

		a.jsonRPC = server.NewJSONRPC(a.conf.RPCServer, jsonRPCServer, a.logger)
		a.logger.Info("json-rpc auth methods are registered")

	case "GRPC":
		grpcServer := grpc.NewServer()
		grpcMethods := Auth.NewAuthServiceGRPC(a.Servises.Auth)

		authPB.RegisterAuthServer(grpcServer, grpcMethods)

		a.jsonRPC = server.NewGRPC(a.conf.RPCServer, grpcServer, a.logger)
		a.logger.Info("grpc auth methods are registered")

	default:
		a.logger.Fatal("unknown server type")
	}

	// возвращаем приложение
	return a
}
