FROM golang:1.20-alpine AS builder
RUN go version
ENV GOPATH=/
ADD . /app/
WORKDIR /app
COPY . .
RUN go mod download
RUN go build -o main cmd/auth/main.go

# Run stage
FROM alpine
WORKDIR /app
ADD . /doc/
COPY --from=builder /app/.env /app/
COPY --from=builder /app .

CMD ["/app/main"]