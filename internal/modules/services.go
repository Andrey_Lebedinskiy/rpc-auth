package modules

import (
	"gitlab.com/Andrey_Lebedinskiy/rpc-auth/internal/infrastructure/component"
	"gitlab.com/Andrey_Lebedinskiy/rpc-auth/internal/modules/auth/service"
	uservice "gitlab.com/Andrey_Lebedinskiy/rpc-auth/internal/modules/user/service"
	"gitlab.com/Andrey_Lebedinskiy/rpc-auth/internal/storages"
)

type Services struct {
	Auth service.Auther
	User uservice.Userer
}

func NewServices(storages *storages.Storages, components *component.Components, user uservice.Userer) *Services {

	return &Services{
		Auth: service.NewAuth(user, storages.Verify, components),
		User: user,
	}
}
