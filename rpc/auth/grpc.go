package Auth

import (
	"context"
	authPB "gitlab.com/Andrey_Lebedinskiy/my-go-rpc/grpc/proto/auth"
	"gitlab.com/Andrey_Lebedinskiy/rpc-auth/internal/modules/auth/service"
	models "gitlab.com/Andrey_Lebedinskiy/rpc-auth/internal/modules/auth/service"
)

type AuthServiceGRPC struct {
	authService service.Auther
	authPB.UnimplementedAuthServer
}

func NewAuthServiceGRPC(authService service.Auther) *AuthServiceGRPC {
	return &AuthServiceGRPC{authService: authService}
}

func (a *AuthServiceGRPC) Register(ctx context.Context, request *authPB.RegisterRequest) (*authPB.RegisterResponse, error) {

	out := a.authService.Register(ctx, models.RegisterIn{
		Email:          request.Email,
		Phone:          request.Phone,
		Password:       request.Password,
		IdempotencyKey: request.IdempotencyKey,
	}, 0)

	return &authPB.RegisterResponse{
		Status:    int64(out.Status),
		ErrorCode: int64(out.ErrorCode),
	}, nil
}

func (a *AuthServiceGRPC) AuthorizeEmail(ctx context.Context, request *authPB.AuthorizeEmailRequest) (*authPB.AuthorizeEmailResponse, error) {

	out := a.authService.AuthorizeEmail(ctx, models.AuthorizeEmailIn{
		Email:          request.GetEmail(),
		Password:       request.GetPassword(),
		RetypePassword: request.GetRetypePassword(),
	})

	return &authPB.AuthorizeEmailResponse{
		UserID:       int64(out.UserID),
		AccessToken:  out.AccessToken,
		RefreshToken: out.RefreshToken,
		ErrorCode:    int64(out.ErrorCode),
	}, nil
}

func (a *AuthServiceGRPC) AuthorizeRefresh(ctx context.Context, request *authPB.AuthorizeRefreshRequest) (*authPB.AuthorizeEmailResponse, error) {
	out := a.authService.AuthorizeRefresh(ctx, models.AuthorizeRefreshIn{UserID: int(request.GetUserID())})
	return &authPB.AuthorizeEmailResponse{
		UserID:       int64(out.UserID),
		AccessToken:  out.AccessToken,
		RefreshToken: out.RefreshToken,
		ErrorCode:    int64(out.ErrorCode),
	}, nil
}

func (a *AuthServiceGRPC) AuthorizePhone(ctx context.Context, request *authPB.AuthorizePhoneRequest) (*authPB.AuthorizeEmailResponse, error) {

	out := a.authService.AuthorizePhone(ctx, models.AuthorizePhoneIn{
		Phone: request.GetPhone(),
		Code:  int(request.GetCode()),
	})

	return &authPB.AuthorizeEmailResponse{
		UserID:       int64(out.UserID),
		AccessToken:  out.AccessToken,
		RefreshToken: out.RefreshToken,
		ErrorCode:    int64(out.ErrorCode),
	}, nil
}

func (a *AuthServiceGRPC) SendPhoneCode(ctx context.Context, request *authPB.SendPhoneCodeRequest) (*authPB.SendPhoneCodeResponse, error) {

	out := a.authService.SendPhoneCode(ctx, models.SendPhoneCodeIn{Phone: request.GetPhone()})

	return &authPB.SendPhoneCodeResponse{
		Phone: out.Phone,
		Code:  int64(out.Code),
	}, nil
}

func (a *AuthServiceGRPC) VerifyEmail(ctx context.Context, request *authPB.VerifyEmailRequest) (*authPB.VerifyEmailResponse, error) {

	out := a.authService.VerifyEmail(ctx, models.VerifyEmailIn{
		Hash:  request.GetHash(),
		Email: request.GetEmail(),
	})

	return &authPB.VerifyEmailResponse{
		Success:   out.Success,
		ErrorCode: int64(out.ErrorCode),
	}, nil
}
